#!/bin/sh
ROOT=$1
SCRIPT_DIR=$(realpath $(dirname $0) )

if [ x$ROOT == "x" ]; then
    echo "Usage: $0 FAKEROOT_DIR"
    exit 1
fi

mkdir -p $ROOT || exit 1

# Create and initilize new package database
apk --root $ROOT add --initdb

# Setup repositories URLs
echo "https://dl-cdn.alpinelinux.org/alpine/v3.19/main" >> $ROOT/etc/apk/repositories
echo "https://dl-cdn.alpinelinux.org/alpine/v3.19/community" >> $ROOT/etc/apk/repositories

# Install keys for Alpine repositories
# After this, we will no longer need the "--allow-untrusted" flag
apk --root $ROOT --allow-untrusted update
apk --root $ROOT --allow-untrusted add alpine-keys
apk --root $ROOT fix
apk --root $ROOT list -I

echo "Fakeroot init done."
