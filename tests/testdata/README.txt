Short summary of files/dirs used by apk:

/etc
/etc/apk
/dev
/dev/console
/dev/null
/dev/random
/dev/urandom
/dev/zero
/lib/apk
/lib/apk/db
/tmp
/var
/var/cache
/var/cache/apk
/var/cache/misc

Reference used: apk-tools/src/database.c:

static int apk_db_create(struct apk_database *db)
{
        int fd;

        mkdirat(db->root_fd, "tmp", 01777);
        mkdirat(db->root_fd, "dev", 0755);
        mknodat(db->root_fd, "dev/null", S_IFCHR | 0666, makedev(1, 3));
        mknodat(db->root_fd, "dev/zero", S_IFCHR | 0666, makedev(1, 5));
        mknodat(db->root_fd, "dev/random", S_IFCHR | 0666, makedev(1, 8));
        mknodat(db->root_fd, "dev/urandom", S_IFCHR | 0666, makedev(1, 9));
        mknodat(db->root_fd, "dev/console", S_IFCHR | 0600, makedev(5, 1));
        mkdirat(db->root_fd, "etc", 0755);
        mkdirat(db->root_fd, "etc/apk", 0755);
        mkdirat(db->root_fd, "lib", 0755);
        mkdirat(db->root_fd, "lib/apk", 0755);
        mkdirat(db->root_fd, "lib/apk/db", 0755);
        mkdirat(db->root_fd, "var", 0755);
        mkdirat(db->root_fd, "var/cache", 0755);
        mkdirat(db->root_fd, "var/cache/apk", 0755);
        mkdirat(db->root_fd, "var/cache/misc", 0755);

        fd = openat(db->root_fd, apk_world_file, O_CREAT|O_RDWR|O_TRUNC|O_CLOEXEC, 0644);
        if (fd < 0)
                return -errno;
        close(fd);
        fd = openat(db->root_fd, apk_installed_file, O_CREAT|O_RDWR|O_TRUNC|O_CLOEXEC, 0644);
        if (fd < 0)
                return -errno;
        close(fd);

        return 0;
}
