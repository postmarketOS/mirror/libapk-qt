// SPDX-License-Identifier: GPL-2.0-or-later

#include <QByteArray>
#include <QCoreApplication>
#include <QDataStream>
#include <QDebug>
#include <QIODevice>

#include <QtApk>

using namespace QtApk;

void test_package()
{
    Package p;
    p.name = QStringLiteral("dummy");
    p.version = QStringLiteral("0.1");
    p.arch = QStringLiteral("aarch64");
    p.license = QStringLiteral("GPL");
    p.origin = QStringLiteral("ffsdfsd");
    p.maintainer = QStringLiteral("dummy");
    p.url = QStringLiteral("https://ya.ru");
    p.description = QStringLiteral("desc");
    p.commit = QStringLiteral("sfsdfsdf");
    p.filename = QStringLiteral("assadsad");
    p.installedSize = 65526;
    p.size = 32768;
    p.buildTime = QDateTime::currentDateTime();

    QByteArray ba;
    QDataStream stream(&ba, QIODevice::ReadWrite);
    stream << p;
    qDebug() << ba;
}

void test_repository()
{
    Repository r;
    r.url = "https://ya.ru";
    r.comment = " comment";
    r.enabled = true;

    QByteArray ba;
    QDataStream stream(&ba, QIODevice::ReadWrite);
    stream << r;
    qDebug() << ba;
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    test_package();
    test_repository();

    return 0;
}
